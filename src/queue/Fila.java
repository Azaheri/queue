package queue;

import java.util.*;

class Celula {
	protected int informacao;
	protected Celula link;

	public Celula() {
		link = null;
		informacao = 0;
	}

	public Celula(int d, Celula n) {
		informacao = d;
		link = n;
	}

	public void setLink(Celula n) {
		link = n;
	}

	// Funcao para definir informacoes para a celula atual
	public void setInformacao(int d) {
		informacao = d;
	}

	// Funcao para pegar o link para a pr�xima celula
	public Celula getLink() {
		return link;
	}

	// Funcao para pegar informacao da celula atual
	public int getInformacao() {
		return informacao;
	}
}

class filaLinkada {
	protected Celula frente, atras;
	public int tamanho;

	public filaLinkada() {
		frente = null;
		atras = null;
		tamanho = 0;
	}

	// Funcao para checar se a fila est� vazia
	public boolean isEmpty() {
		return frente == null;
	}

	// Funcao para pegar o tamanho da fila
	public int getTamanho() {
		return tamanho;
	}

	// Funcao para inserir um elemento na fila
	public void inserir(int informacao) {
		Celula novaC = new Celula(informacao, null);
		if (atras == null) {
			frente = novaC;
			atras = novaC;
		} else {
			atras.setLink(novaC);
			atras = atras.getLink();
		}
		tamanho++;
	}

	// Funcao para remover um elemento na frente da fila
	public int remove() {
		if (isEmpty())
			throw new NoSuchElementException("Underflow Exception");
		Celula selec = frente;
		frente = selec.getLink();
		if (frente == null)
			atras = null;
		tamanho--;
		return selec.getInformacao();
	}

	// Funcao para checar os elemento da fila
	public int ver() {

		if (isEmpty())
			throw new NoSuchElementException("Underflow Exception");
		return frente.getInformacao();
	}

	// Funcao para mostrar o status da fila
	public void mostrar() {
		System.out.print("\nFila= ");
		if (tamanho == 0) {
			System.out.print("Vazio\n");
			return;
		}
		Celula selec = frente;
		while (selec != atras.getLink()) {
			System.out.print(selec.getInformacao() + " ");
			selec = selec.getLink();
		}
		System.out.println();
	}
}

public class Fila {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		filaLinkada fl = new filaLinkada();

		System.out.println("Fila Linkada\n");
		char esc;
		System.out.println("\nOpera��es da Fila");
		System.out.println("1. Inserir");
		System.out.println("2. Remover");
		System.out.println("3. Ver");
		System.out.println("4. Verificar Vazio");
		System.out.println("5. Tamanho");

		do {
			System.out.print("Digite sua escolha: ");
			int escolha = scan.nextInt();
			switch (escolha) {

			case 1:
				System.out.print("Insira um n�mero inteiro: ");
				fl.inserir(scan.nextInt());
				break;
			case 2:
				try {
					System.out.println("Elemento Removido= " + fl.remove());
				} catch (Exception e) {
					System.out.println("Erro: " + e.getMessage());
				}
				break;
			case 3:
				try {
					System.out.println("Ver Elemento= " + fl.ver());
				} catch (Exception e) {
					System.out.println("Erro: " + e.getMessage());
				}
				break;
			case 4:
				System.out.println("Status Vazio= " + fl.isEmpty());
				break;

			case 5:
				System.out.println("Tamanho= " + fl.getTamanho());
				break;

			default:
				System.out.println("Entrada errada \n ");
				break;
			}
			// mostrar a fila
			fl.mostrar();

			System.out.print("\nDeseja continuar (Digite S ou N): ");
			esc = scan.next().charAt(0);
		} while (esc == 'S' || esc == 's');
	}
}